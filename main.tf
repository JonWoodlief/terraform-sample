locals {
    nullcheck = var.aminull == null ? "yes" : "no"
}
data "template_file" "test" {
  template = "Howdy ${var.name}! null? ${local.nullcheck}"
}
